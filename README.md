# python100days

### 今天学习了git的用法
```rb
# git clone git@gitlab.com:shushuitie2017/python100days.git
```
### 首先下载仓库代码

```rb
# git init
### 本地git初始化

# git add *
### 添加本地修改的文件
# git commit -m "initial commit"
### 添加上传修改的信息
# git push origin main
# git push origin kerry
### 根据不同分支名 更新仓库内容

# git clone  -b ブランチ名　https://github.com/ユーザー名/リポジトリ名.git
```
