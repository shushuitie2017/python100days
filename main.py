# ステープ❶環境構築
# Anaconda インストール
# https://www.anaconda.com/
# vscode インストール
# https://code.visualstudio.com/

# cmd 起動

# dir or ls フォルダーを探す
# cd　フォルダーname　指定
# code . vscode 起動

# terminal 

# 練習❶現在の時間を出力
from datetime import datetime
time = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
if time:
    print(time)  


# 簡単にいうと、上記のコードは以下の意味です。

# １）１行目で、datetimeというライブラリをインポートしている
# ２）２行目で、datetime.now() という命令をして、現在の時刻を取得している
# ３）それに .strftime(“%Y/%m/%d %H:%M:%S”)を繋げて、以下のようなかたちに整形している
# （〇年/〇月/〇日 〇時：〇分：〇秒）
# ４）上記の整形した結果を、timeという変数に代入している
# ５）３行目で、print()でtimeに代入した内容を表示している

